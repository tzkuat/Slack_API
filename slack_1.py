import os
import json
import datetime
from slackclient import SlackClient

import sys

#Entrez keyword
#keyword = raw_input("Enter the keyword here : ")

#Ou utilisez le en argument
for arg in sys.argv:
	keyword = arg

slack_token =  "your_slack_token"
sc = SlackClient(slack_token)
res = sc.api_call(
	"search.messages",
	query=keyword
)

test = json.dumps(res, indent=4)
json2 = json.loads(test)

#Recuperation du message
test2 = json.dumps(json2["messages"], indent=4)
json3 = json.loads(test2)
test3 = json.dumps(json3["matches"], indent=4)
json4 = json.loads(test3)
test4 = json.dumps(json4, indent=4)
json_slack = test4
#json_1 = json_slack.replace("[", "")
#json_2 = json_1.replace("]", "")
json_final = json.loads(json_slack)

#Affichage
print "Voici le nom de la personne :"
print(json_final["username"])
print "Voici le texte complet :"
print(json_final["text"])

'''
field_list = res['messages']
for fields in field_list:
	print(fields['matches'][0]['user'])'''
